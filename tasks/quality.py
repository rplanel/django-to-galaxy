from invoke import task


@task
def black_format(c):
    """Run black format."""
    c.run("black django_to_galaxy/")


@task
def black_check(c):
    """Run black check."""
    c.run("black django_to_galaxy/ --check")


@task
def lint(c):
    """Run flake8."""
    c.run("flake8 django_to_galaxy/")


@task(black_format, lint)
def all(c):
    """Run all quality tests."""
    pass
