from django.contrib import admin

from simple_app.models import SomeInputFile, SomeOtherInputFile


@admin.register(SomeInputFile)
class SomeInputFileAdmin(admin.ModelAdmin):
    list_display = (
        "id", "name", "file_type", "file_path",
    )


@admin.register(SomeOtherInputFile)
class SomeOtherInputFileAdmin(admin.ModelAdmin):
    list_display = (
        "id", "name", "file_type", "file",
    )