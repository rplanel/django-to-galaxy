import factory
from factory.django import DjangoModelFactory

from django_to_galaxy.models.galaxy_output_file import GalaxyOutputFile

from .invocation import InvocationFactory


class GalaxyOutputFileFactory(DjangoModelFactory):
    class Meta:
        model = GalaxyOutputFile

    galaxy_id = factory.Faker("md5")
    workflow_name = factory.Faker("bothify", text="Workflow-?###")
    galaxy_state = "new"
    history_name = factory.Faker("bothify", text="File-?###")
    src = "hda"
    invocation = factory.SubFactory(InvocationFactory)
