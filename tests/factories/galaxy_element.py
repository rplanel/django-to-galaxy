import factory
from factory.django import DjangoModelFactory

from django_to_galaxy.models.galaxy_element import GalaxyElement

from .galaxy_user import GalaxyUserFactory


class GalaxyElementFactory(DjangoModelFactory):
    class Meta:
        model = GalaxyElement

    galaxy_id = factory.Faker("md5")
    name = factory.Faker("bothify", text="Galaxy Element-?###")
    annotation = factory.Faker("sentence")
    published = factory.Faker("boolean")
    galaxy_owner = factory.SubFactory(GalaxyUserFactory)
