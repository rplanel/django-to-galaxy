from datetime import datetime

from django_to_galaxy.settings import settings


def generate_galaxy_time() -> str:
    return datetime.now().strftime(settings.GALAXY_TIME_FORMAT)
