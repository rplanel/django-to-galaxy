from unittest.mock import patch, Mock, MagicMock

from django.test import TestCase
from django.contrib import messages
from django.contrib.admin.sites import AdminSite
from django.http import HttpResponseRedirect

from django_to_galaxy.admin.galaxy_user import GalaxyUserAdmin
from django_to_galaxy.models import GalaxyUser

from tests.factories.galaxy_user import GalaxyUserFactory


SINGULAR_STR = "singular"
PLURAL_STR = "plural"


class MockRequest:
    method = "GET"


class MockPostItem:
    def getlist(self, name: str):
        return ["test_id"]


class MockPostRequest(MockRequest):
    method = "POST"
    POST = MockPostItem()


class TestGalaxyUserAdmin(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.galaxy_user = GalaxyUserFactory()

    def setUp(self):
        self.site = AdminSite()

    def test_hide_api_key(self):
        # Given
        galaxy_user_admin = GalaxyUserAdmin(GalaxyUser, self.site)
        # When
        self.galaxy_user.api_key = "352998794846518976542256"
        expected_api_key = "3529****************2256"
        # Then
        self.assertEqual(
            galaxy_user_admin.hide_api_key(self.galaxy_user), expected_api_key
        )

    def test_get_number_histories(self):
        # Given
        galaxy_user_admin = GalaxyUserAdmin(GalaxyUser, self.site)
        # When
        link_str = "link_history"
        expected_link = (
            f'<a href="{link_str}?galaxy_owner__id={self.galaxy_user.id}">0</a>'
        )
        mock_reverse = Mock()
        mock_reverse.return_value = link_str
        with patch("django_to_galaxy.admin.galaxy_user.reverse", mock_reverse):
            # Then
            self.assertEqual(
                galaxy_user_admin.get_number_histories(self.galaxy_user), expected_link
            )

    def test_get_number_workflows(self):
        # Given
        galaxy_user_admin = GalaxyUserAdmin(GalaxyUser, self.site)
        # When
        link_str = "link_workflow"
        expected_link = (
            f'<a href="{link_str}?galaxy_owner__id={self.galaxy_user.id}">0</a>'
        )
        mock_reverse = Mock()
        mock_reverse.return_value = link_str
        with patch("django_to_galaxy.admin.galaxy_user.reverse", mock_reverse):
            # Then
            self.assertEqual(
                galaxy_user_admin.get_number_workflows(self.galaxy_user), expected_link
            )

    def test_get_message_singular_history_creation(self):
        # Given
        galaxy_user_admin = GalaxyUserAdmin(GalaxyUser, self.site)
        # When
        expected_string = (
            f"1 {SINGULAR_STR} ({self.galaxy_user.email}"
            f"->{self.galaxy_user.galaxy_instance.url})."
        )
        # - mock message_user method
        galaxy_user_admin.message_user = Mock()
        galaxy_user_admin.message_user.return_value = None
        # Then
        galaxy_user_admin._get_message_history_creation(
            MockRequest(),
            [self.galaxy_user],
            SINGULAR_STR,
            PLURAL_STR,
            messages.SUCCESS,
        )
        self.assertEqual(
            galaxy_user_admin.message_user.call_args_list[0][0][1], expected_string
        )

    def test_get_message_plural_history_creation(self):
        # Given
        galaxy_user_admin = GalaxyUserAdmin(GalaxyUser, self.site)
        # When
        expected_string = (
            f"2 {PLURAL_STR} ({self.galaxy_user.email}->{self.galaxy_user.galaxy_instance.url}, "
            f"{self.galaxy_user.email}->{self.galaxy_user.galaxy_instance.url})."
        )
        # - mock message_user method
        galaxy_user_admin.message_user = Mock()
        galaxy_user_admin.message_user.return_value = None
        # Then
        galaxy_user_admin._get_message_history_creation(
            MockRequest(),
            [self.galaxy_user, self.galaxy_user],
            SINGULAR_STR,
            PLURAL_STR,
            messages.SUCCESS,
        )
        self.assertEqual(
            galaxy_user_admin.message_user.call_args_list[0][0][1], expected_string
        )

    def test_create_history(self):
        # Given
        galaxy_user_admin = GalaxyUserAdmin(GalaxyUser, self.site)
        # When
        to_success_creation = Mock()
        to_success_creation.create_history.return_value = None
        to_fail_creation = Mock()
        to_fail_creation.create_history.side_effect = Exception()
        fake_queryset = [to_success_creation, to_fail_creation]
        # - mock method for message
        galaxy_user_admin._get_message_history_creation = Mock()
        galaxy_user_admin._get_message_history_creation.return_value = None
        # - call the method
        galaxy_user_admin.create_history(MockRequest(), fake_queryset)
        # Then
        to_success_creation.create_history.assert_called_once()
        to_fail_creation.create_history.assert_called_once()
        galaxy_user_admin._get_message_history_creation.assert_called()
        self.assertEqual(galaxy_user_admin._get_message_history_creation.call_count, 2)

    def test_import_workflows_more_than_one_user(self):
        # Given
        galaxy_user_admin = GalaxyUserAdmin(GalaxyUser, self.site)
        # When
        fake_queryset = Mock()
        fake_queryset.count.return_value = 2
        # - mock method for message
        galaxy_user_admin.message_user = Mock()
        galaxy_user_admin.message_user.return_value = None
        # - call the method
        galaxy_user_admin.import_workflows(MockRequest(), fake_queryset)
        # Then
        galaxy_user_admin.message_user.assert_called_once()

    def test_import_workflows_online_instance(self):
        # Given
        galaxy_user_admin = GalaxyUserAdmin(GalaxyUser, self.site)
        # When
        self.galaxy_user.galaxy_instance.is_online = lambda: True
        expected_output = HttpResponseRedirect(
            f"{self.galaxy_user.id}/import_workflows/"
        )
        fake_queryset = MagicMock()
        fake_queryset.count.return_value = 1
        fake_queryset.__getitem__.return_value = self.galaxy_user
        # - mock method for message
        galaxy_user_admin.message_user = Mock()
        galaxy_user_admin.message_user.return_value = None
        # Then
        self.assertEqual(
            galaxy_user_admin.import_workflows(MockRequest(), fake_queryset).url,
            expected_output.url,
        )

    def test_import_workflows_offline_instance(self):
        # Given
        galaxy_user_admin = GalaxyUserAdmin(GalaxyUser, self.site)
        galaxy_user_admin.message_user = Mock()
        # When
        self.galaxy_user.galaxy_instance.is_online = lambda: False
        fake_queryset = MagicMock()
        fake_queryset.count.return_value = 1
        fake_queryset.__getitem__.return_value = self.galaxy_user
        # - mock method for message
        galaxy_user_admin.message_user = Mock()
        galaxy_user_admin.message_user.return_value = None
        # Then
        self.assertIsNone(
            galaxy_user_admin.import_workflows(MockRequest(), fake_queryset),
        )
        galaxy_user_admin.message_user.assert_called_once()

    def test_import_workflows_view(self):
        # Given
        galaxy_user_admin = GalaxyUserAdmin(GalaxyUser, self.site)
        # When
        # - mock context
        galaxy_user_admin.admin_site.each_context = lambda x: {}
        # - mock render
        mock_render = Mock()
        mock_render = lambda *args, **kwargs: (args, kwargs)  # noqa
        # - mock available workflows
        mock_workflow = Mock()
        mock_workflow.galaxy_id = "test_id"
        mock_available_workflows = Mock()
        mock_available_workflows.return_value = [(mock_workflow, [])]
        # Then
        with patch("django_to_galaxy.admin.galaxy_user.render", mock_render), patch(
            "django_to_galaxy.admin.galaxy_user.GalaxyUser.get_available_workflows",
            mock_available_workflows,
        ):
            # Then
            test_output = galaxy_user_admin._import_workflows_view(
                MockRequest(), self.galaxy_user.galaxy_instance.id
            )
            args = test_output[0]
            self.assertEqual(
                args[1], "admin/import_workflows.html"
            )  # rendering template
            kwargs = test_output[1]
            self.assertEqual(kwargs["context"]["galaxy_user"], self.galaxy_user)
            self.assertEqual(kwargs["context"]["existing_workflows"], [])
            self.assertEqual(kwargs["context"]["new_workflows"], [(mock_workflow, [])])

    def test_import_workflows_view_post(self):
        # Given
        galaxy_user_admin = GalaxyUserAdmin(GalaxyUser, self.site)
        galaxy_user_admin.message_user = lambda *args, **kwargs: None
        # When
        # - mock context
        galaxy_user_admin.admin_site.each_context = lambda x: {}
        # - mock render
        mock_render = Mock()
        mock_render = lambda *args, **kwargs: (args, kwargs)  # noqa
        # - mock available workflows
        mock_workflow = Mock()
        mock_workflow.save.return_value = None
        mock_workflow.galaxy_id = "test_id"
        mock_workflow.galaxy_workflow = Mock(inputs={})
        mock_available_workflows = Mock()
        mock_available_workflows.return_value = [(mock_workflow, [])]
        # Then
        with patch("django_to_galaxy.admin.galaxy_user.render", mock_render), patch(
            "django_to_galaxy.admin.galaxy_user.GalaxyUser.get_available_workflows",
            mock_available_workflows,
        ):
            # Then
            test_output = galaxy_user_admin._import_workflows_view(
                MockPostRequest(), self.galaxy_user.galaxy_instance.id
            )
            args = test_output[0]
            self.assertEqual(
                args[1], "admin/import_workflows.html"
            )  # rendering template
            kwargs = test_output[1]
            self.assertEqual(kwargs["context"]["galaxy_user"], self.galaxy_user)
            self.assertEqual(kwargs["context"]["existing_workflows"], [mock_workflow])
            self.assertEqual(kwargs["context"]["new_workflows"], [])
            mock_workflow.save.assert_called()
