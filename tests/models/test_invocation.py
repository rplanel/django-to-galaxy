from unittest.mock import Mock, MagicMock, patch

from django.test import TestCase

from django_to_galaxy.models.invocation import Invocation, DONE

from tests.factories.invocation import InvocationFactory


class TestInvocation(TestCase):
    def setUp(self):
        self.invocation = InvocationFactory()

    def test_galaxy_invocation(self):
        # Given
        invocation = Invocation.objects.get(galaxy_id=self.invocation.galaxy_id)
        # When
        # - Mock obj_gi
        mock_obj_gi = MagicMock()
        mock_obj_gi.invocations.get.return_value = True
        invocation.workflow.galaxy_owner.obj_gi = mock_obj_gi
        # Then
        self.assertTrue(invocation.galaxy_invocation)
        mock_obj_gi.invocations.get.assert_called_once()

    def test_synchronize(self):
        # Given
        invocation = Invocation.objects.get(galaxy_id=self.invocation.galaxy_id)
        # When
        # - Mock galaxy_invocation
        mock_galaxy_invocation = Mock()
        mock_galaxy_invocation.state = "new_state"
        mock_get_galaxy_invocation = Mock()
        mock_get_galaxy_invocation.return_value = mock_galaxy_invocation
        invocation._get_galaxy_invocation = mock_get_galaxy_invocation
        # Then
        self.assertNotEqual(invocation.galaxy_state, "new_state")
        invocation.synchronize()
        self.assertEqual(invocation.galaxy_state, "new_state")

    def test_percentage_done_state_done(self):
        # Given
        invocation = Invocation.objects.get(galaxy_id=self.invocation.galaxy_id)
        # When
        invocation.status = DONE
        # Then
        self.assertEqual(invocation.percentage_done, 100.0)

    def test_percentage_done_all_done(self):
        # Given
        invocation = Invocation.objects.get(galaxy_id=self.invocation.galaxy_id)
        # When
        # - Mock galaxy_invocation
        mock_galaxy_invocation = Mock()
        mock_galaxy_invocation.step_jobs_summary.return_value = [
            {"states": {"ok": 1}},
            {"states": {"ok": 1}},
        ]
        mock_get_galaxy_invocation = Mock()
        mock_get_galaxy_invocation.return_value = mock_galaxy_invocation
        invocation._get_galaxy_invocation = mock_get_galaxy_invocation
        # Then
        self.assertEqual(invocation.percentage_done, 100.0)

    def test_percentage_done_half_done(self):
        # Given
        invocation = Invocation.objects.get(galaxy_id=self.invocation.galaxy_id)
        # When
        # - Mock galaxy_invocation
        mock_galaxy_invocation = Mock()
        mock_galaxy_invocation.step_jobs_summary.return_value = [
            {"states": {"ok": 1}},
            {"states": {"running": 1}},
        ]
        mock_get_galaxy_invocation = Mock()
        mock_get_galaxy_invocation.return_value = mock_galaxy_invocation
        invocation._get_galaxy_invocation = mock_get_galaxy_invocation
        # Then
        self.assertEqual(invocation.percentage_done, 50.0)

    def test_percentage_done_error_half_done(self):
        # Given
        invocation = Invocation.objects.get(galaxy_id=self.invocation.galaxy_id)
        # When
        # - Mock galaxy_invocation
        mock_galaxy_invocation = Mock()
        mock_galaxy_invocation.step_jobs_summary.return_value = [
            {"states": {"ok": 1}},
            {"states": {"error": 1}},
        ]
        mock_get_galaxy_invocation = Mock()
        mock_get_galaxy_invocation.return_value = mock_galaxy_invocation
        invocation._get_galaxy_invocation = mock_get_galaxy_invocation
        # Then
        self.assertEqual(invocation.status, "running")
        self.assertEqual(invocation.percentage_done, 50.0)
        self.assertEqual(invocation.status, "error")

    def test_percentage_done_paused_half_done(self):
        # Given
        invocation = Invocation.objects.get(galaxy_id=self.invocation.galaxy_id)
        # When
        # - Mock galaxy_invocation
        mock_galaxy_invocation = Mock()
        mock_galaxy_invocation.step_jobs_summary.return_value = [
            {"states": {"ok": 1}},
            {"states": {"paused": 1}},
        ]
        mock_get_galaxy_invocation = Mock()
        mock_get_galaxy_invocation.return_value = mock_galaxy_invocation
        invocation._get_galaxy_invocation = mock_get_galaxy_invocation
        # Then
        self.assertEqual(invocation.status, "running")
        self.assertEqual(invocation.percentage_done, 50.0)
        self.assertEqual(invocation.status, "paused")

    def test_percentage_done_paused_and_error(self):
        # Given
        invocation = Invocation.objects.get(galaxy_id=self.invocation.galaxy_id)
        # When
        # - Mock galaxy_invocation
        mock_galaxy_invocation = Mock()
        mock_galaxy_invocation.step_jobs_summary.return_value = [
            {"states": {"error": 1}},
            {"states": {"paused": 1}},
        ]
        mock_get_galaxy_invocation = Mock()
        mock_get_galaxy_invocation.return_value = mock_galaxy_invocation
        invocation._get_galaxy_invocation = mock_get_galaxy_invocation
        # Then
        self.assertEqual(invocation.status, "running")
        self.assertEqual(invocation.percentage_done, 0.0)
        self.assertEqual(invocation.status, "error")

    def test_job_id_to_tools(self):
        # Given
        invocation = Invocation.objects.get(galaxy_id=self.invocation.galaxy_id)
        # When
        # - Mock galaxy_invocation
        mock_galaxy_invocation = Mock()
        job_id = "ppouet"
        tool_id = "masse"
        mock_galaxy_invocation.step_jobs_summary.return_value = [
            {"states": {"ok": 1}, "id": job_id},
        ]
        mock_get_galaxy_invocation = Mock()
        mock_get_galaxy_invocation.return_value = mock_galaxy_invocation
        invocation._get_galaxy_invocation = mock_get_galaxy_invocation
        # - Mock obj_gi from workflow
        mock_obj_gi = Mock()
        # ---- Galaxy Job
        mock_galaxy_job = Mock()
        mock_galaxy_job.wrapped = {
            "tool_id": tool_id,
        }
        mock_obj_gi.jobs.get.return_value = mock_galaxy_job
        # ---- Galaxy Tool
        tool_description = {"tool_id": tool_id, "info_tool": "tool"}
        mock_galaxy_tool = Mock()
        mock_galaxy_tool.wrapped = tool_description
        mock_obj_gi.tools.get.return_value = mock_galaxy_tool
        invocation.workflow.galaxy_owner.obj_gi = mock_obj_gi
        # - Expected output
        expected_dict = {job_id: tool_description}
        # Then
        self.assertDictEqual(invocation.job_id_to_tools, expected_dict)

    def test_detailed_step_jobs_summary(self):
        # Given
        invocation = Invocation.objects.get(galaxy_id=self.invocation.galaxy_id)
        # When
        # - Mock galaxy_invocation
        job_id = "pouet"
        tool_dict = {"tool_id": "super_tool"}
        invocation._job_id_to_tools = {job_id: tool_dict}
        mock_galaxy_invocation = Mock()
        mock_galaxy_invocation.step_jobs_summary.return_value = [
            {"states": {"ok": 1}, "id": job_id},
        ]
        mock_get_galaxy_invocation = Mock()
        mock_get_galaxy_invocation.return_value = mock_galaxy_invocation
        invocation._get_galaxy_invocation = mock_get_galaxy_invocation
        expected_list = [
            {"states": {"ok": 1}, "id": job_id, "tool": tool_dict},
        ]
        # Then
        self.assertListEqual(invocation.detailed_step_jobs_summary, expected_list)

    def test_create_output_files_empty_outputs(self):
        # Given
        invocation = Invocation.objects.get(galaxy_id=self.invocation.galaxy_id)
        # When
        # - Mock galaxy_invocation
        invocation_description = {"id": "test123", "outputs": {}}
        mock_galaxy_invocation = Mock()
        mock_galaxy_invocation.wrapped = invocation_description
        mock_get_galaxy_invocation = Mock()
        mock_get_galaxy_invocation.return_value = mock_galaxy_invocation
        invocation._get_galaxy_invocation = mock_get_galaxy_invocation
        # Then
        with self.assertLogs(level="WARNING") as cm:  # noqa
            invocation.create_output_files()

    def test_create_output_files_no_outputs(self):
        # Given
        invocation = Invocation.objects.get(galaxy_id=self.invocation.galaxy_id)
        # When
        # - Mock galaxy_invocation
        invocation_description = {"id": "test123"}
        mock_galaxy_invocation = Mock()
        mock_galaxy_invocation.wrapped = invocation_description
        mock_get_galaxy_invocation = Mock()
        mock_get_galaxy_invocation.return_value = mock_galaxy_invocation
        invocation._get_galaxy_invocation = mock_get_galaxy_invocation
        # Then
        with self.assertLogs(level="WARNING") as cm:  # noqa
            invocation.create_output_files(max_retry=1)

    def test_create_output_files(self):
        # Given
        invocation = Invocation.objects.get(galaxy_id=self.invocation.galaxy_id)
        # When
        # - Mock galaxy_invocation
        invocation_description = {
            "id": "test123",
            "outputs": {
                "line number": {
                    "src": "hda",
                    "id": "8493be72e99244cf",
                    "workflow_step_id": "79e0c1b61bf65c3f",
                }
            },
        }
        mock_galaxy_invocation = Mock()
        mock_galaxy_invocation.wrapped = invocation_description
        mock_get_galaxy_invocation = Mock()
        mock_get_galaxy_invocation.return_value = mock_galaxy_invocation
        invocation._get_galaxy_invocation = mock_get_galaxy_invocation
        # - Mock Invocation models and method
        mock_galaxy_output_file = Mock()
        mock_galaxy_output_file_instance = Mock()
        mock_galaxy_output_file_instance.save = Mock()
        mock_galaxy_output_file.return_value = mock_galaxy_output_file_instance
        # Then
        with patch(
            "django_to_galaxy.models.invocation.GalaxyOutputFile",
            mock_galaxy_output_file,
        ):
            invocation.create_output_files(max_retry=1)
        mock_galaxy_output_file.assert_called_once()
        mock_galaxy_output_file_instance.save.assert_called_once()

    def test_update_output_files_empty_outputs(self):
        # Given
        invocation = Invocation.objects.get(galaxy_id=self.invocation.galaxy_id)
        # When
        # - Mock galaxy_invocation
        invocation_description = {"id": "test123", "outputs": {}}
        mock_galaxy_invocation = Mock()
        mock_galaxy_invocation.wrapped = invocation_description
        mock_get_galaxy_invocation = Mock()
        mock_get_galaxy_invocation.return_value = mock_galaxy_invocation
        invocation._get_galaxy_invocation = mock_get_galaxy_invocation
        # Then
        with self.assertLogs(level="WARNING") as cm:  # noqa
            invocation.update_output_files()

    def test_update_output_files_no_outputs(self):
        # Given
        invocation = Invocation.objects.get(galaxy_id=self.invocation.galaxy_id)
        # When
        # - Mock galaxy_invocation
        invocation_description = {"id": "test123"}
        mock_galaxy_invocation = Mock()
        mock_galaxy_invocation.wrapped = invocation_description
        mock_get_galaxy_invocation = Mock()
        mock_get_galaxy_invocation.return_value = mock_galaxy_invocation
        invocation._get_galaxy_invocation = mock_get_galaxy_invocation
        # Then
        with self.assertLogs(level="WARNING") as cm:  # noqa
            invocation.update_output_files()

    def test_update_existing_output_files(self):
        # Given
        invocation = Invocation.objects.get(galaxy_id=self.invocation.galaxy_id)
        # When
        # - Mock galaxy_invocation
        invocation_description = {
            "id": "test123456",
            "outputs": {
                "line number": {
                    "src": "hda",
                    "id": "fake_id",
                    "workflow_step_id": "79e0c1b61bf65c3f",
                }
            },
        }
        mock_galaxy_invocation = Mock()
        mock_galaxy_invocation.wrapped = invocation_description
        mock_get_galaxy_invocation = Mock()
        mock_get_galaxy_invocation.return_value = mock_galaxy_invocation
        invocation._get_galaxy_invocation = mock_get_galaxy_invocation
        # - Mock Invocation models and method
        mock_galaxy_output_file = Mock()
        # --- Mock filter & exists
        mock_filter = Mock()
        mock_filter.exists.return_value = True
        mock_get = Mock()
        mock_get.return_value = True
        mock_galaxy_output_file.objects.filter.return_value = mock_filter
        mock_galaxy_output_file.objects.get.return_value = mock_get
        # Then
        with patch(
            "django_to_galaxy.models.invocation.GalaxyOutputFile",
            mock_galaxy_output_file,
        ):
            invocation.update_output_files()
        mock_galaxy_output_file.objects.get.assert_called_once()

    def test_update_new_output_files(self):
        # Given
        invocation = Invocation.objects.get(galaxy_id=self.invocation.galaxy_id)
        # When
        # - Mock galaxy_invocation
        invocation_description = {
            "id": "test123456",
            "outputs": {
                "line number": {
                    "src": "hda",
                    "id": "fake_id",
                    "workflow_step_id": "79e0c1b61bf65c3f",
                }
            },
        }
        mock_galaxy_invocation = Mock()
        mock_galaxy_invocation.wrapped = invocation_description
        mock_get_galaxy_invocation = Mock()
        mock_get_galaxy_invocation.return_value = mock_galaxy_invocation
        invocation._get_galaxy_invocation = mock_get_galaxy_invocation
        # - Mock Invocation models and method
        mock_galaxy_output_file = Mock()
        mock_galaxy_output_file_instance = Mock()
        mock_galaxy_output_file_instance.save = Mock()
        mock_galaxy_output_file.return_value = mock_galaxy_output_file_instance
        # --- Mock filter & exists
        mock_filter = Mock()
        mock_filter.exists.return_value = False
        mock_galaxy_output_file.objects.filter.return_value = mock_filter
        # Then
        with patch(
            "django_to_galaxy.models.invocation.GalaxyOutputFile",
            mock_galaxy_output_file,
        ):
            invocation.update_output_files()
        mock_galaxy_output_file.assert_called_once()
        mock_galaxy_output_file_instance.save.assert_called_once()

    def test_str(self):
        # Given
        invocation = Invocation.objects.get(galaxy_id=self.invocation.galaxy_id)
        # Then
        self.assertEqual(
            str(invocation),
            f"{self.invocation.galaxy_id} [{self.invocation.workflow.name}]",
        )

    def test_repr(self):
        # Given
        invocation = Invocation.objects.get(galaxy_id=self.invocation.galaxy_id)
        # Then
        self.assertEqual(
            repr(invocation),
            f"Invocation: {self.invocation.galaxy_id} [{self.invocation.workflow.name}]",
        )
