from unittest.mock import Mock, MagicMock

from django.test import TestCase

from django_to_galaxy.models.galaxy_output_file import GalaxyOutputFile

from tests.factories.galaxy_output_file import GalaxyOutputFileFactory


class TestInvocation(TestCase):
    def setUp(self):
        self.galaxy_output_file = GalaxyOutputFileFactory()

    def test_galaxy_dataset(self):
        # Given
        galaxy_output_file = GalaxyOutputFile.objects.get(
            galaxy_id=self.galaxy_output_file.galaxy_id
        )
        # When
        # - Mock obj_gi
        mock_galaxy_history = MagicMock()
        mock_galaxy_history.get_dataset.return_value = {"id": "test123"}
        galaxy_output_file.invocation.history._get_galaxy_history = (
            lambda: mock_galaxy_history
        )
        # Then
        self.assertTrue(galaxy_output_file.galaxy_dataset)
        mock_galaxy_history.get_dataset.assert_called_once()

    def test_synchronize(self):
        # Given
        galaxy_output_file = GalaxyOutputFile.objects.get(
            galaxy_id=self.galaxy_output_file.galaxy_id
        )
        # When
        # When
        # - Mock obj_gi
        mock_galaxy_dataset = Mock()
        mock_galaxy_dataset.state = "new_state"
        mock_galaxy_dataset.name = "new test name"
        mock_galaxy_history = MagicMock()
        mock_galaxy_history.get_dataset.return_value = mock_galaxy_dataset
        galaxy_output_file.invocation.history._get_galaxy_history = (
            lambda: mock_galaxy_history
        )
        # Then
        self.assertNotEqual(galaxy_output_file.galaxy_state, "new_state")
        self.assertNotEqual(galaxy_output_file.history_name, "new test name")
        galaxy_output_file.synchronize()
        self.assertEqual(galaxy_output_file.galaxy_state, "new_state")
        self.assertEqual(galaxy_output_file.history_name, "new test name")

    def test_str(self):
        # Given
        galaxy_output_file = GalaxyOutputFile.objects.get(
            galaxy_id=self.galaxy_output_file.galaxy_id
        )
        # Then
        self.assertEqual(
            str(galaxy_output_file),
            f"{self.galaxy_output_file.history_name} (from {self.galaxy_output_file.invocation.galaxy_id})",
        )
