# Generated by Django 4.0.3 on 2022-07-19 09:09

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    replaces = [
        ("django_to_galaxy", "0001_initial"),
        (
            "django_to_galaxy",
            "0002_alter_galaxyuser_galaxy_instance_alter_history_owner_and_more",
        ),
        ("django_to_galaxy", "0003_remove_history_latest_payload_and_more"),
        ("django_to_galaxy", "0004_alter_history_unique_together_and_more"),
        ("django_to_galaxy", "0005_alter_workflow_unique_together_invocation"),
        ("django_to_galaxy", "0006_rename_owner_history_galaxy_owner_and_more"),
        ("django_to_galaxy", "0007_history_create_time"),
    ]

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="GalaxyInstance",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("url", models.URLField(max_length=100)),
                ("name", models.CharField(max_length=100, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name="GalaxyUser",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "email",
                    models.EmailField(
                        help_text="Email used for the Galaxy account", max_length=254
                    ),
                ),
                ("api_key", models.CharField(max_length=50)),
                (
                    "galaxy_instance",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="users",
                        to="django_to_galaxy.galaxyinstance",
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="History",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("galaxy_id", models.CharField(max_length=50)),
                ("name", models.CharField(max_length=200)),
                ("annotation", models.CharField(max_length=200)),
                ("published", models.BooleanField()),
                ("state", models.CharField(max_length=100)),
                (
                    "galaxy_owner",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="histories",
                        to="django_to_galaxy.galaxyuser",
                    ),
                ),
                (
                    "create_time",
                    models.DateTimeField(default=django.utils.timezone.now),
                ),
            ],
            options={
                "abstract": False,
                "verbose_name_plural": "Histories",
                "unique_together": {("galaxy_id", "galaxy_owner")},
            },
        ),
        migrations.CreateModel(
            name="Workflow",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("galaxy_id", models.CharField(max_length=50)),
                ("name", models.CharField(max_length=200)),
                ("annotation", models.CharField(max_length=200)),
                ("published", models.BooleanField()),
                (
                    "galaxy_owner",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="workflows",
                        to="django_to_galaxy.galaxyuser",
                    ),
                ),
            ],
            options={
                "abstract": False,
                "unique_together": set(),
            },
        ),
        migrations.CreateModel(
            name="Invocation",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("galaxy_id", models.CharField(max_length=50)),
                ("state", models.CharField(max_length=200)),
                (
                    "history",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="histories",
                        to="django_to_galaxy.history",
                    ),
                ),
                (
                    "workflow",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="workflows",
                        to="django_to_galaxy.workflow",
                    ),
                ),
            ],
        ),
    ]
