"""Handle library versioning."""
version_info = (0, 6, 4)
__version__ = ".".join(str(c) for c in version_info)
